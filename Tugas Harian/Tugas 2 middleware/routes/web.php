<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth', 'RoleCheck:superadmin']], function () {
    Route::get('/route-1', 'MiddlewareController@superadmin')->name('superadmin');
});
Route::group(['middleware' => ['auth', 'RoleCheck:superadmin,admin']], function () {
    Route::get('/route-2', 'MiddlewareController@admin')->name('admin');
});
Route::group(['middleware' => ['auth', 'RoleCheck:superadmin,admin,guest']], function () {
    Route::get('/route-3', 'MiddlewareController@guest')->name('guest');
});
