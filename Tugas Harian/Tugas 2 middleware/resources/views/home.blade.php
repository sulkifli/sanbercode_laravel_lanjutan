@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                </div>
                <div class="mt-4 ml-2">
                <a href="{{route('superadmin')}}"><button class="btn btn-primary">Menu superadmin</button></a>
                    <a href="{{route('admin')}}"><button class="btn btn-success">Menu admin</button></a>
                    <a href="{{route('guest')}}"><button class="btn btn-danger">Menu guest</button></a>
                </div>
            </div>
        </div>
       
    </div>
    
</div>
@endsection
