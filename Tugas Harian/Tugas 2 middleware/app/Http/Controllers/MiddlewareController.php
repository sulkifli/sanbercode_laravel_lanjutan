<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MiddlewareController extends Controller
{
    public function superadmin()
    {
        echo "ini halaman untuk super admin";
    }

    public function admin()
    {
        echo "ini halaman untuk admin";
    }


    public function guest()
    {
        echo "ini halaman untuk guest";
    }
}
