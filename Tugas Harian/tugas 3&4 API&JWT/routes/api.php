<?php


Route::post('login', 'LoginController@login');

Route::group(['middleware' => ['auth:api', 'RoleCheck:admin']], function () {
    Route::post('create_buku', 'BukuController@store');
    Route::post('create_peminjaman', 'PeminjamanController@store');
    Route::post('create_pengembalian', 'PengembalianController@store');
    Route::post('register', 'RegisterController@register');
});


Route::group(['middleware' => ['auth:api', 'RoleCheck:admin,mahasiswa']], function () {
    Route::get('user', 'UserController@user');
});

Route::put('update_pengembalian/{id}', 'PengembalianController@update');
Route::post('logout', 'LogoutController@logout');
