<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'tgl_peminjaman' =>  'required',
            'waktu_tengang_pengembalian' => 'required',
            'user_id' => 'required',
            'buku_id' => 'required',
        ]);
        $peminjaman = new Peminjaman;
        $peminjaman->tgl_peminjaman  = request('tgl_peminjaman');
        $peminjaman->waktu_tengang_pengembalian  = request('waktu_tengang_pengembalian');
        $peminjaman->user_id  = request('user_id');
        $peminjaman->buku_id  = request('buku_id');
        $peminjaman->save();
        return response('Peminjaman berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
