<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        request()->validate([
            'name' => ['string', 'min:3', 'max:30', 'required'],
            'email' => ['email', 'required'],
            'password' => ['min:6', 'required'],
            'role' => ['required'],
        ]);

        $user = new User;
        $user->name  = request('name');
        $user->nim  = request('nim');
        $user->fakultas  = request('fakultas');
        $user->jurusan  = request('jurusan');
        $user->no_hp  = request('no_hp');
        $user->no_wa  = request('no_wa');
        $user->email  = request('email');
        $user->role  = request('role');
        $user->password  = bcrypt(request('password'));

        $user->save();
        return response('Terimakasih Telah Mendaftar Di Perpustakaan Kami');
    }
}
