<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengembalian;

class PengembalianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'tgl_pengembalian' =>  'required',
            'status_ontime' => 'required',
            'peminjaman_id' => 'required',
        ]);
        $pengembalian = new Pengembalian;
        $pengembalian->tgl_pengembalian  = request('tgl_pengembalian');
        $pengembalian->status_ontime  = request('status_ontime');
        $pengembalian->peminjaman_id  = request('peminjaman_id');
        $pengembalian->save();
        return response('Pengembalian berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'tgl_pengembalian' =>  'required',
            'status_ontime' => 'required',
        ]);

        $pengembalian = Pengembalian::findOrFail($id);

        $pengembalian->tgl_pengembalian  = request('tgl_pengembalian');
        $pengembalian->status_ontime  = request('status_ontime');
        $pengembalian->save();

        return response('Pengembalian berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
